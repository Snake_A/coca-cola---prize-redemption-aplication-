import { Component, OnInit } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";
import * as email from "nativescript-email";

@Component({
    selector: "Settings",
    moduleId: module.id,
    templateUrl: "./settings.component.html",
    styleUrls: ["./settings.component.css"]
})
export class SettingsComponent implements OnInit {
    text: string = "";
    composeOptions: email.ComposeOptions;
    sent: boolean = false;

    constructor() {
        // Use the component constructor to inject providers.
    }

    ngOnInit(): void {
        // Init your component properties here.
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }

    sendEmail() {
        this.composeOptions = {
            to: ["coca-cola.support@mailinator.com"],
            subject: "Support request",
            body: this.text
        };

        email.available().then((available) => {
            if (available) {
                email.compose(this.composeOptions);
            }
            
        }).then(() => {
            this.text = "";
            this.sent = true;
        });
    }
}
