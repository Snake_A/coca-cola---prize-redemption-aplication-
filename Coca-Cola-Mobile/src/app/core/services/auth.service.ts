import { Injectable } from "@angular/core";
import { StorageService } from "./storage.service";
import { Observable } from "rxjs";
import { HttpClient } from "@angular/common/http";
import { JwtHelperService } from "@auth0/angular-jwt";
import { tap } from "rxjs/operators";
import { JWTService } from "./jwt.service";

@Injectable({
  providedIn: "root"
})
export class AuthService {

  constructor(
    private readonly storage: StorageService,
    private readonly jwtHelper: JwtHelperService,
    private readonly http: HttpClient,
    private readonly jwtService: JWTService
  ) { }

  isAuthenticated(): boolean {
    const token = this.storage.get("token");
    // Check whether the token is expired and return
    // true or false
    if (!!token) {
      return !this.jwtHelper.isTokenExpired(token);
    }

    return false;
  }

  login(username: string, password: string): Observable<any> {
    return this.http.post("http://10.0.2.2:3000/session", { username, password }).pipe(
      tap((res: any) => {
        this.storage.set("token", res.token);
        const user = this.decodeToken(res.token);
        this.storage.set("username", user.username);
        this.storage.set("role", user.roles[0].name);
      }
      ));
  }

  logout() {
    return this.http.delete("http://10.0.2.2:3000/session").pipe(
      tap((res: any) => {
        this.storage.remove("token");
        this.storage.remove("username");
        this.storage.remove("role");
      }
      ));
  }

  private decodeToken(token: string): any {
    return this.jwtService.JWTDecoder(token);
  }
}
