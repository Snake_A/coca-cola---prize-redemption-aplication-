import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";

@Injectable({
    providedIn: "root"
})
export class RecordsDataService {
    constructor(private readonly http: HttpClient) { }

    getRecords(): Observable<any> {
        return this.http.get<any>(`http://10.0.2.2:3000/records/personal`);
    }
}
