# Coca-Cola Prize Redemption

  ![](https://gitlab.com/Snake_A/coca-cola---prize-redemption-aplication-/raw/Dev/Coca-Cola-Mobile/App_Resources/Android/src/main/res/drawable-hdpi/colabarcode.png)

## Structure

  
The Prize Redemption project has three functional parts:

  

### NestJS back-end server

For more information about the API click here:
[API Docs](https://gitlab.com/Snake_A/coca-cola---prize-redemption-aplication-/blob/master/coca-cola-api/README.md)

The server is hosted in Heroku for demonstration purposes:
[coca-cola-backend](https://coca-cola-backend.herokuapp.com/)

### Angular 7 web administrator panel

For more information about the web SPA read here:
[Web SPA Docs](https://gitlab.com/Snake_A/coca-cola---prize-redemption-aplication-/blob/master/Coca-Cola-Web/README.md)

The web SPA is hosted in Heroku for demonstration purposes:
[coca-cola-prize-redemption](https://coca-cola-prize-redemption.herokuapp.com/)

### NativeScript with Angular 7 application

You can find more about the mobile app here:
[Mobile App Docs](https://gitlab.com/Snake_A/coca-cola---prize-redemption-aplication-/blob/master/Coca-Cola-Mobile/README.md)

The mobile APK can be downloaded here demonstration purposes:
[coca-cola-prize-redemption.APK](https://drive.google.com/open?id=1F7Tla8M_INKTYQ2Gt1ls0NsYR-VPOID2)