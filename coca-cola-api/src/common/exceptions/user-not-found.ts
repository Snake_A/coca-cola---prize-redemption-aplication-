import { EntityNotFoundException } from './entity-not-found';

export class UserNotFound extends EntityNotFoundException {}
