import { BadRequestException } from '@nestjs/common';

export class RedemptionRecordBadRequest extends BadRequestException {}
