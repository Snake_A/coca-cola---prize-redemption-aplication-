import { BadRequestException } from './bad-request';

export class OutletBadRequest extends BadRequestException {}
