import { OutletDTO } from './../outlet/outlet.dto';
import { CustomerDTO } from './../customer/customer.dto';
import { Expose, Type } from 'class-transformer';
import { ResponseRedemptionCodeDTO } from '../redemption-code/response-redemption-code.dto';
import { RoleDTO } from '../role/role.dto';

export class UserDTO {
    @Expose()
    id: string;

    @Expose()
    username: string;

    @Expose()
    firstName: string;

    @Expose()
    lastName: string;

    @Expose({name: '__customer__'})
    @Type(() => CustomerDTO)
    customer: CustomerDTO;

    @Expose({name: '__outlet__'})
    @Type(() => OutletDTO)
    outlet: OutletDTO;

    @Expose({name: '__redemptionCodes__'})
    @Type(() => ResponseRedemptionCodeDTO)
    redemptionCodes: ResponseRedemptionCodeDTO;

    @Expose()
    dateCreated: Date;

    @Expose()
    @Type(() => RoleDTO)
    roles: RoleDTO;
}
