import { RedemptionCodeStatus } from './../../common/enums/redemption-code-status-type';
import { IsDate, IsEnum, IsString } from 'class-validator';

export class UpdateRedemptionCodeDTO {
    @IsEnum(RedemptionCodeStatus)
    status: RedemptionCodeStatus;
}
