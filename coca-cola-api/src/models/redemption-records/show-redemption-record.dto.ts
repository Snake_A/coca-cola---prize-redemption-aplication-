import { ShowRedemptionCodeDTO } from './../redemption-code/show-redemption-code.dto';
import { Expose, Type } from "class-transformer";
import { UserDTO } from '../user/user.dto';
import { RedemptionCodeStatus } from '../../common/enums/redemption-code-status-type';
import { OutletDTO } from '../outlet/outlet.dto';
import { CustomerDTO } from '../customer/customer.dto';

export class ShowRedemptionRecordDTO {
    @Expose({name: '__redemptionCode__'})
    @Type(() => ShowRedemptionCodeDTO)
    redemptionCode: ShowRedemptionCodeDTO;

    @Expose({name: '__updatedByUser__' })
    @Type(() => UserDTO)
    updatedByUser: UserDTO;

    @Expose({name: '__customer__'})
    @Type(() => CustomerDTO)
    customer: CustomerDTO;

    @Expose({name: '__outlet__'})
    @Type(() => OutletDTO)
    outlet: OutletDTO;

    @Expose()
    dateCreated: Date;

    @Expose()
    status: RedemptionCodeStatus;

    @Expose()
    info: string;
}
