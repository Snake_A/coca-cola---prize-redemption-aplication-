import { IsString, Length } from 'class-validator';

export class CreateOutletDTO {
    @IsString()
    @Length(2)
    name: string;

    @IsString()
    @Length(2)
    address: string;
}
