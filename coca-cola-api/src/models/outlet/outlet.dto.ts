import { Expose, Type } from 'class-transformer';
import { CustomerDTO } from '../customer/customer.dto';
import { UserDTO } from '../user/user.dto';

export class OutletDTO {
    @Expose()
    id: string;

    @Expose()
    name: string;

    @Expose()
    address: string;

    @Expose({name: '__customer__'})
    @Type(() => CustomerDTO)
    customer: CustomerDTO;

    @Expose({name: '__users__'})
    @Type(() => UserDTO)
    users: UserDTO[];

    @Expose()
    dateCreated: Date;
}
