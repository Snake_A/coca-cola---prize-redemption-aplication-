import { IsString, Length, IsNotEmpty } from 'class-validator';

export class EditOutletDTO {
    @IsString()
    @Length(2)
    name: string;

    @IsString()
    @Length(2)
    address: string;

    // @IsString()
    // @IsNotEmpty()
    // customerId: string;
}
