import { RedemptionRecordModule } from './redemption-record/redemption-record.module';
import { AuthModule } from './auth/auth.module';
import { UserModule } from './users/user.module';
import { CustomerModule } from './customers/customers.module';
import { CoreModule } from 'src/core/core.module';
import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigModule } from './config/config.module';
import { ConfigService } from './config/config.service';
import { OutletModule } from './outlet/outlet.module';
import { RedemptionCodeModule } from './redemption-code/redemption-code.module';

@Module({
  imports: [
    CoreModule,
    CustomerModule,
    UserModule,
    OutletModule,
    AuthModule,
    RedemptionCodeModule,
    RedemptionRecordModule,
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: async (configService: ConfigService) => ({
        type: configService.dbType as any,
        host: configService.dbHost,
        port: configService.dbPort,
        username: configService.dbUsername,
        password: configService.dbPassword,
        database: configService.dbName,
        entities: ['./src/data/entities/*.ts'],
        subscribers: ['./src/common/subscribers/*.ts'],
      }),
    }),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
