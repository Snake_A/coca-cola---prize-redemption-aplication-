import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity('roles')
export class Roles {
    @PrimaryGeneratedColumn()
    id: string;
    @Column('nvarchar')
    name: string;
}
