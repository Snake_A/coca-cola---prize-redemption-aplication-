import { Entity, PrimaryGeneratedColumn, Column, OneToMany, CreateDateColumn, ManyToOne } from 'typeorm';
import { Outlet } from './outlet.entity';
import { User } from './user.entity';
import { RedemptionCode } from './redemption-code.entity';
import { RedemptionRecord } from './redemption-record.entity';

@Entity('customer')
export class Customer {

    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column('nvarchar')
    name: string;

    @CreateDateColumn()
    dateCreated: Date;

    @OneToMany(type => Outlet, outlets => outlets.customer)
    outlets: Promise<Outlet[]>;

    @OneToMany(type => User, users => users.customer)
    users: Promise<User[]>;

    @OneToMany(type => RedemptionCode, redemptionCode => redemptionCode.redeemedFromCustomer)
    redemptionCodes: Promise<RedemptionCode[]>;

    @OneToMany(type => RedemptionRecord, redemptionRecord => redemptionRecord.customer)
    redemptionRecords: Promise<RedemptionRecord[]>;

    @Column({default: false})
    isDeleted: boolean;
}
