import { Customer } from '../../data/entities/customer.entity';
import 'reflect-metadata';
import { RedemptionCodeStatus } from '../../common/enums/redemption-code-status-type';
import { createConnection } from 'typeorm';
import { RedemptionCode } from '../entities/redemption-code.entity';
import { User } from '../entities/user.entity';
import { Outlet } from '../entities/outlet.entity';
import * as bcrypt from 'bcrypt';
import { Roles } from '../entities/roles.entity';

const main = async () => {
    const connection = await createConnection();

    const redemptionCodeRepository = connection.manager.getRepository(RedemptionCode);
    const userRepository = connection.manager.getRepository(User);
    const customerRepository = connection.manager.getRepository(Customer);
    const rolesRepository = connection.manager.getRepository(Roles);
    const outletRepository = connection.manager.getRepository(Outlet);

    // Creating roles
    const user = await rolesRepository.findOne({
        where: { name: 'user' },
    });

    if (!user) {
        const newRole = await rolesRepository.create();
        newRole.name = 'user';
        await rolesRepository.save(newRole);
    } else {
        console.log('Role "User" already exists');
    }

    const admin = await rolesRepository.findOne({
        where: { name: 'admin' },
    });

    if (!user) {
        const newRole = await rolesRepository.create();
        newRole.name = 'admin';
        await rolesRepository.save(newRole);
    } else {
        console.log('Role "Admin" already exists');
    }

    // Creation of Customer
    const lidl = await customerRepository.findOne({
        where: { name: 'Lidl' },
    });

    if (!lidl) {
        const newCustomer = await customerRepository.create();
        newCustomer.name = 'Lidl';
        await customerRepository.save(newCustomer);
    } else {
        console.log('Lidl already exists in DB!');
    }

    const billa = await customerRepository.findOne({
        where: { name: 'Billa' },
    });

    if (!billa) {
        const newCustomer = await customerRepository.create();
        newCustomer.name = 'Billa';
        await customerRepository.save(newCustomer);
    } else {
        console.log('Billa already exists in DB!');
    }

    // Creation of Outlet
    const lidlOutlet = await outletRepository.findOne({
        where: { name: 'Lidl Дружба2' },
    });

    if (!lidlOutlet) {
        const newOutlet = await outletRepository.create();
        newOutlet.name = 'Lidl Дружба2';
        newOutlet.address = 'Дружба 2, ул. Димитър Пешев 30';
        const customer = await customerRepository.findOne({
            where: { name: 'Lidl' },
        });
        newOutlet.customer = Promise.resolve(customer);
        await outletRepository.save(newOutlet);
    } else {
        console.log('Lidl Дружба2 already exist in DB!');
    }

    const billaOutlet = await outletRepository.findOne({
        where: { name: 'Billa Младост2' },
    });

    if (!billaOutlet) {
        const newOutlet = await outletRepository.create();
        newOutlet.name = 'Billa Младост2';
        newOutlet.address = 'Младост 2, Бул.Александър Малинов';
        const customer = await customerRepository.findOne({
            where: { name: 'Billa' },
        });
        newOutlet.customer = Promise.resolve(customer);
        await outletRepository.save(newOutlet);
    } else {
        console.log('Billa Младост2 already exist in DB!');
    }

    // Creation of Users
    const Admin = await userRepository.findOne({
        where: { username: 'Admin' },
    });

    if (!Admin) {
        const newUser = await userRepository.create();
        newUser.username = 'Admin';
        newUser.firstName = 'Георги';
        newUser.lastName = 'Петров';
        newUser.password = await bcrypt.hash('Aaaaaz@1', 10);
        const userRole = await rolesRepository.findOne({
            where: { name: 'admin' },
        });
        newUser.roles = [userRole];
        await userRepository.save(newUser);
    } else {
        console.log('Admin is already in DB');
    }

    const user1 = await userRepository.findOne({
        where: { username: 'John' },
    });

    if (!user1) {
        const newUser = await userRepository.create();
        newUser.username = 'John';
        newUser.firstName = 'Иван';
        newUser.lastName = 'Колев';
        newUser.password = await bcrypt.hash('Aaaaaz@1', 10);
        const userRole = await rolesRepository.findOne({
            where: { name: 'user' },
        });
        newUser.roles = [userRole];
        const customer = await customerRepository.findOne({
            where: { name: 'Lidl' },
        });
        newUser.customer = Promise.resolve(customer);
        const outlet = await outletRepository.findOne({
            where: { name: 'Lidl Дружба2' },
        });
        newUser.outlet = Promise.resolve(outlet);
        await userRepository.save(newUser);
    } else {
        console.log('John is already in DB');
    }

    const user2 = await userRepository.findOne({
        where: { username: 'Peter' },
    });

    if (!user2) {
        const newUser = await userRepository.create();
        newUser.username = 'Peter';
        newUser.firstName = 'Петър';
        newUser.lastName = 'Иванов';
        newUser.password = await bcrypt.hash('Aaaaaz@1', 10);
        const userRole = await rolesRepository.findOne({
            where: { name: 'user' },
        });
        newUser.roles = [userRole];
        const customer = await customerRepository.findOne({
            where: { name: 'Billa' },
        });
        newUser.customer = Promise.resolve(customer);
        const outlet = await outletRepository.findOne({
            where: { name: 'Billa Младост2' },
        });
        newUser.outlet = Promise.resolve(outlet);
        await userRepository.save(newUser);
    } else {
        console.log('Peter is already in DB');
    }

    const sasho = await userRepository.findOne({
        where: { username: 'Alexander' },
    });

    if (!sasho) {
        const newUser = await userRepository.create();
        newUser.username = 'Alexander';
        newUser.firstName = 'Александър';
        newUser.lastName = 'Петров';
        newUser.password = await bcrypt.hash('Aaaaaz@1', 10);
        const userRole = await rolesRepository.findOne({
            where: { name: 'user' },
        });
        newUser.roles = [userRole];
        const customer = await customerRepository.findOne({
            where: { name: 'Billa' },
        });
        newUser.customer = Promise.resolve(customer);
        const outlet = await outletRepository.findOne({
            where: { name: 'Billa Младост2' },
        });
        newUser.outlet = Promise.resolve(outlet);
        await userRepository.save(newUser);
    } else {
        console.log('Alexander is already in DB');
    }

    // Creation of redemption codes
    const redemptionCode = await redemptionCodeRepository.findOne({
        where: { code: 1234567891011 },
    });

    if (!redemptionCode) {
        const newRedemptionCode = new RedemptionCode();
        newRedemptionCode.code = '1234567891011';
        newRedemptionCode.status = RedemptionCodeStatus.valid;
        newRedemptionCode.dateCreated = new Date();
        newRedemptionCode.reported = false;
        await redemptionCodeRepository.save(newRedemptionCode);
    } else {
        console.log('Redemption code with code 1234567891011 is already in DB!');
    }

    const redemptionCode2 = await redemptionCodeRepository.findOne({
        where: { code: 2345678910111 },
    });

    if (!redemptionCode2) {
        const newRedemptionCode = new RedemptionCode();
        newRedemptionCode.code = '2345678910111';
        newRedemptionCode.status = RedemptionCodeStatus.valid;
        newRedemptionCode.dateCreated = new Date();
        newRedemptionCode.reported = false;
        await redemptionCodeRepository.save(newRedemptionCode);
    } else {
        console.log('Redemption code with code 2345678910111 is already in DB!');
    }

    const redemptionCode3 = await redemptionCodeRepository.findOne({
        where: { code: 3456789101112 },
    });

    if (!redemptionCode3) {
        const newRedemptionCode = new RedemptionCode();
        newRedemptionCode.code = '3456789101112';
        newRedemptionCode.status = RedemptionCodeStatus.valid;
        newRedemptionCode.dateCreated = new Date();
        newRedemptionCode.reported = false;
        await redemptionCodeRepository.save(newRedemptionCode);
    } else {
        console.log('Redemption code with code 3456789101112 is already in DB!');
    }

    connection.close();

};

main().catch(console.error);
