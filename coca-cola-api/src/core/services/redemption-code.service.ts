import { ShowRedemptionRecordDTO } from './../../models/redemption-records/show-redemption-record.dto';
import { UpdateRedemptionCodeDTO } from './../../models/redemption-code/update-redemption-code.dto';
import { ShowRedemptionCodeDTO } from './../../models/redemption-code/show-redemption-code.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { RedemptionCode } from '../../data/entities/redemption-code.entity';
import { Repository } from 'typeorm';
import { plainToClass } from 'class-transformer';
import { User } from '../../data/entities/user.entity';
import { Outlet } from '../../data/entities/outlet.entity';
import { Customer } from '../../data/entities/customer.entity';
import { RedemptionCodeStatus } from '../../common/enums/redemption-code-status-type';
import { RedemptionCodeBadRequest } from '../../common/exceptions/redemption-code-bad-request';
import { RedemptionCodeNotFound } from '../../common/exceptions/redemption-code-not-found';
import { RedemptionRecord } from '../../data/entities/redemption-record.entity';

export class RedemptionCodeService {

    constructor(
        @InjectRepository(RedemptionCode) private readonly redemptionCodeRepository: Repository<RedemptionCode>,
        @InjectRepository(User) private readonly userRepository: Repository<User>,
        @InjectRepository(RedemptionRecord) private readonly redemptionRecordRepository: Repository<RedemptionRecord>,
        @InjectRepository(Outlet) private readonly outletRepository: Repository<Outlet>,
        @InjectRepository(Customer) private readonly customerRepository: Repository<Customer>,
    ) { }

    async getAllCodes(): Promise<ShowRedemptionCodeDTO[]> {
        const allCodes = await this.redemptionCodeRepository.find({
            order: {
                id: 'ASC',
            },
            relations: ['redeemedAtOutlet', 'redeemedFrom', 'redeemedFromCustomer'],
        });

        return plainToClass(ShowRedemptionCodeDTO, allCodes, { excludeExtraneousValues: true });
    }

    async getCodeById(id: string): Promise<ShowRedemptionCodeDTO> {
        // Find the code by id
        const code = await this.redemptionCodeRepository.findOne({
            where: { code: id },
            relations: ['redeemedAtOutlet', 'redeemedFrom', 'redeemedFromCustomer'],
        });

        // Throw if the code does not exist
        if (!code) {
            throw new RedemptionCodeNotFound('This redemption code was not found!');
        }

        // Conversion
        return plainToClass(ShowRedemptionCodeDTO, code, { excludeExtraneousValues: true });
    }

    async updateRedemptionCode(id: string, content: UpdateRedemptionCodeDTO, user: User): Promise<ShowRedemptionCodeDTO> {
        // Search the code by id
        const redemptionCode: RedemptionCode = await this.redemptionCodeRepository.findOne({
            where: { code: id },
            relations: ['redeemedAtOutlet', 'redeemedFrom', 'redeemedFromCustomer'],
        });

        // Throw if the code does not exist
        if (!redemptionCode) {
            throw new RedemptionCodeNotFound('This redemption code was not found!');
        }

        // Check if the code is already redeemed
        if (redemptionCode.status === RedemptionCodeStatus.redeemed) {
            throw new RedemptionCodeBadRequest('The status of this redemption code cannot be changed!');
        }

        // Search the user, outlet and customer
        const userById: User = await this.userRepository.findOne({
            where: { id: user.id, isDeleted: false },
        });
        const outlet = await userById.outlet;
        const customer = await userById.customer;

        // Change the status and fields of redemption code entity
        redemptionCode.status = content.status;
        redemptionCode.redeemedFrom = Promise.resolve(userById);
        redemptionCode.redeemedAtOutlet = Promise.resolve(outlet);
        redemptionCode.redeemedFromCustomer = Promise.resolve(customer);
        redemptionCode.redeemedAt = new Date();

        // Create redemption record
        const newRedemptionRecord = await this.redemptionRecordRepository.create();
        newRedemptionRecord.updatedByUser = Promise.resolve(userById);
        newRedemptionRecord.outlet = Promise.resolve(outlet);
        newRedemptionRecord.customer = Promise.resolve(customer);
        newRedemptionRecord.redemptionCode = Promise.resolve(redemptionCode);
        newRedemptionRecord.status = content.status;
        newRedemptionRecord.dateCreated = new Date();
        newRedemptionRecord.info = `The status of the redemption code was changed to ${content.status} at ${new Date()}`;

        // Save in repository
        const savedRedemptionRecord = await this.redemptionRecordRepository.save(newRedemptionRecord);

        // Save in repository
        const savedRedemptionCode = await this.redemptionCodeRepository.save(redemptionCode);

        // Conversion of redemption code only
        return plainToClass(ShowRedemptionCodeDTO, savedRedemptionCode, { excludeExtraneousValues: true });
    }

    async reportRedemptionCode(id: string, user: User): Promise<ShowRedemptionCodeDTO> {
        // Search the code by id
        const redemptionCode = await this.redemptionCodeRepository.findOne({
            where: { code: id },
            relations: ['redeemedAtOutlet', 'redeemedFrom', 'redeemedFromCustomer'],
        });

        // Throw if the code does not exist
        if (!redemptionCode) {
            throw new RedemptionCodeNotFound('This redemption code was not found!');
        }

        // Change the report status of the redemption code
        redemptionCode.reported = true;

        // Search the user, outlet and customer
        const userById: User = await this.userRepository.findOne({
            where: { id: user.id, isDeleted: false },
        });
        const outlet = await userById.outlet;
        const customer = await userById.customer;

        // Create redemption record
        const newRedemptionRecord = await this.redemptionRecordRepository.create();
        newRedemptionRecord.updatedByUser = Promise.resolve(userById);
        newRedemptionRecord.outlet = Promise.resolve(outlet);
        newRedemptionRecord.customer = Promise.resolve(customer);
        newRedemptionRecord.redemptionCode = Promise.resolve(redemptionCode);
        const reported = RedemptionCodeStatus.reported;
        newRedemptionRecord.status = reported;
        newRedemptionRecord.dateCreated = new Date();
        newRedemptionRecord.info = `The status of the redemption code was changed to ${reported} at ${new Date()}`;

        // Save in repository
        const savedRedemptionRecord = await this.redemptionRecordRepository.save(newRedemptionRecord);

        // Save in repository and conversion
        const savedRedemptionCode = await this.redemptionCodeRepository.save(redemptionCode);
        return plainToClass(ShowRedemptionCodeDTO, savedRedemptionCode, { excludeExtraneousValues: true });
    }
}
