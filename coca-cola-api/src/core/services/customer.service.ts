import { CustomerBadRequest } from './../../common/exceptions/customer-bad-request';
import { CustomerDTO } from './../../models/customer/customer.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Customer } from 'src/data/entities/customer.entity';
import { Repository } from 'typeorm';
import { CreateCustomerDTO } from 'src/models/customer/create-customer.dto';
import { plainToClass } from 'class-transformer';
import { CreateCustomerResponseDTO } from 'src/models/customer/create-response.dto';
import { EditCustomerDTO } from 'src/models/customer/edit-customer.dto';
import { CustomerNotFound } from '../../common/exceptions/customer-not-found';
import { OutletService } from './outlet.service';
import { Outlet } from 'src/data/entities/outlet.entity';

export class CustomerService {
    constructor(
        @InjectRepository(Customer) private readonly customerRepository: Repository<Customer>,
        private outletService: OutletService,
    ) { }

    async getAllCustomers(): Promise<CustomerDTO[]> {
        const allCustomers = await this.customerRepository.find({
            where: { isDeleted: false },
            relations: ['outlets'],
            order: { name: 'ASC' },
        });

        // Throw error if no customer were found.
        if (allCustomers.length === 0) {
            // throw new CustomerNotFound('No customers found!');
            return [];
        }

        const customers = allCustomers.map((customer: any) => {
            const outlets = customer.__outlets__;
            if (outlets) {
                const newOutlets = outlets.filter((outlet: Outlet) => outlet.isDeleted === false);
                customer.__outlets__ = newOutlets;
            }
            return customer;
        });

        // Conversion
        return plainToClass(CustomerDTO, customers, { excludeExtraneousValues: true });
    }

    async getCustomerById(id: string): Promise<CustomerDTO> {
        const customerId = await this.customerRepository.findOne({
            where: { id, isDeleted: false },
        });

        // Throw error if customer was not found.
        if (!customerId) {
            throw new CustomerNotFound('Customer was not found!');
        }

        // Conversion
        return plainToClass(CustomerDTO, customerId, { excludeExtraneousValues: true });
    }

    async createCustomer(newCustomer: CreateCustomerDTO): Promise<CreateCustomerResponseDTO> {
        // Throw error if customer name already exists
        const customer = await this.customerRepository.findOne({
            where: { name: newCustomer.name },
        });
        if (customer) {
            throw new CustomerBadRequest('Customer name already exists!');
        }
        const savedCustomer = await this.customerRepository.save(newCustomer);

        // Conversion
        return plainToClass(CreateCustomerResponseDTO, savedCustomer, { excludeExtraneousValues: true });
    }

    async editCustomer(id: string, content: EditCustomerDTO): Promise<EditCustomerDTO> {
        // Customer search
        const customerId = await this.customerRepository.findOne({
            where: { id },
        });

        // Throw error if customer was not found.
        if (!customerId) {
            throw new CustomerNotFound('Customer was not found!');
        }
        customerId.name = content.name;
        const savedCustomer = await this.customerRepository.save(customerId);

        // Conversion
        return plainToClass(EditCustomerDTO, savedCustomer, { excludeExtraneousValues: true });
    }

    async deleteCustomer(id: string): Promise<CustomerDTO> {
        // Customer search
        const customerId = await this.customerRepository.findOne({
            where: { id, isDeleted: false },
        });

        // Throw error if customer was not found.
        if (!customerId) {
            throw new CustomerNotFound('Customer was not found!');
        }

        // Change isDeleted field and save in repository
        customerId.isDeleted = true;
        const savedCustomer = await this.customerRepository.save(customerId);

        const outlets = await customerId.outlets;
        outlets.forEach((outlet: Outlet) => {
            this.outletService.deleteOutlet(outlet.id);
        });

        // Conversion
        return plainToClass(CustomerDTO, savedCustomer, { excludeExtraneousValues: true });
    }
}
