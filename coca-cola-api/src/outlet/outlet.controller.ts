import { RolesGuard } from './../common/guards/roles.guard';
import { AuthGuard } from '@nestjs/passport';
import { Controller, Get, Param, Post, Put, Body, ValidationPipe, Delete, Query, UseFilters, UseGuards } from '@nestjs/common';
import { OutletService } from 'src/core/services/outlet.service';
import { OutletDTO } from 'src/models/outlet/outlet.dto';
import { CreateOutletDTO } from 'src/models/outlet/create-outlet.dto';
import { EditOutletDTO } from 'src/models/outlet/edit-outlet.dto';
import { BadRequestFilter } from 'src/common/filters/bad-request.filter';
import { EntityNotFoundFilter } from 'src/common/filters/entity-not-found.filter';
import { JwtAuthGuard } from '../common/guards/jwt-auth.guard';
import { Roles } from '../common/decorators/roles.decorator';

@Roles('admin')
@Controller()
@UseGuards(AuthGuard(), JwtAuthGuard, RolesGuard)
@UseFilters(BadRequestFilter, EntityNotFoundFilter)
export class OutletController {
    constructor(private readonly outletService: OutletService) { }

    @Get('customers/:customerId/outlets')
    async getAllOutletsForACustomer(
        @Query() query: any,
        @Param('customerId') customerId: string,
    ): Promise<OutletDTO[]> {
        return await this.outletService.getAllOutletsForACustomer(query, customerId);
    }

    @Get('outlets')
    async getAllOutlets(
        @Query() query: any,
    ): Promise<OutletDTO[]> {
        return await this.outletService.getAllOutlets(query);
    }

    @Get(['customers/:customerId/outlets/:id', 'outlets/:id'])
    async getOutletById(
        @Param('id') id: string,
    ): Promise<OutletDTO> {
        return await this.outletService.getOutletById(id);
    }

    @Post('customers/:customerId/outlets')
    async createOutlet(
        @Body(
            new ValidationPipe(
                {
                    whitelist: true,
                    transform: true,
                },
            ),
        )
        newOutlet: CreateOutletDTO,
        @Param('customerId') customerId: string,
    ): Promise<OutletDTO | string> {
        return await this.outletService.createOutlet(newOutlet, customerId);
    }

    @Put(['customers/:customerId/outlets/:id', 'outlets/:id'])
    async editOutlet(
        @Param('id') id: string,
        @Body(
            new ValidationPipe(
                {
                    whitelist: true,
                    transform: true,
                },
            ),
        )
        content: EditOutletDTO,
    ): Promise<OutletDTO> {
        return await this.outletService.editOutlet(id, content);
    }

    @Delete(['outlets/:id', 'customers/:customerId/outlets/:id'])
    async deleteOutlet(
        @Param('id') id: string,
    ): Promise<OutletDTO> {
        return await this.outletService.deleteOutlet(id);
    }
}
