import { AuthModule } from './../auth/auth.module';
import { Module } from '@nestjs/common';
import { OutletController } from './outlet.controller';
import { CoreModule } from '../core/core.module';

@Module({
    imports: [CoreModule, AuthModule],
    controllers: [OutletController],
})
export class OutletModule {}
