import { UserBadRequest } from './../common/exceptions/user-bad-request';
import { BadRequestException } from './../common/exceptions/bad-request';
import { UserDTO } from './../models/user/user.dto';
import { LoginDTO } from './../models/user/login.dto';
import { UsersService } from './../core/services/users.service';
import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { JwtPayload } from '../core/interfaces/jwt.payload';
import { BlacklistService } from './blacklist/blacklist.service';

@Injectable()
export class AuthService {

    constructor(
        private readonly jwtService: JwtService,
        private readonly userService: UsersService,
        private readonly blacklistService: BlacklistService,
        ) {}

    async login(loginContent: LoginDTO): Promise<string> {
        if (await this.validatePassword(loginContent)) {
            const user = await this.userService.getUserByUsername(loginContent.username);
            return await this.jwtService.signAsync({ ...user });
        } else {
            throw new UserBadRequest('Username or password is incorrect!');
        }
    }

    async logout(token: string): Promise<string> {
        return await this.blacklistService.blacklist(token);
    }

    async validateUser(payload: JwtPayload): Promise<UserDTO> {
        return await this.userService.validateUser(payload);
    }

    async validatePassword(user: LoginDTO): Promise<boolean> {
        return await this.userService.validatePassword(user);
    }
}
