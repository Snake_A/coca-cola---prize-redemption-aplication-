import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NotFoundComponent } from './shared/not-found/not-found.component';
import { ErrorComponent } from './shared/error/error.component';
import { AuthGuardService as AuthGuard } from '../../src/app/core/services/auth-guard.service';
import { AdminGuardService as AdminGuard } from '../../src/app/core/services/admin-guard.service';
import { UsersResolver } from './resolvers/users.resolver';
import { CoreModule } from './core/core.module';
import { OutletsResolver } from './resolvers/outlets.resolver';
import { CustomersResolver } from './resolvers/customers.resolver';
import { RedemptionsResolver } from './resolvers/redemptions.resolver';
import { HomeComponent } from './home/home.component';


const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: 'login', loadChildren: './auth/auth.module#AuthModule' },
  {
    path: 'users', loadChildren: './user/user.module#UserModule',
    resolve: {
      users: UsersResolver,
      outletsResolve: OutletsResolver,
    },
    canActivate: [AuthGuard, AdminGuard]
  },
  {
    path: 'outlets', loadChildren: './outlet/outlet.module#OutletModule',
    resolve: {
      outlets: OutletsResolver,
      customers: CustomersResolver,
    },
    canActivate: [AuthGuard, AdminGuard]
  },
  {
    path: 'customers', loadChildren: './customer/customer.module#CustomerModule',
    resolve: {
      customers: CustomersResolver,
    },
    canActivate: [AuthGuard, AdminGuard]
  },
  {
    path: 'records', loadChildren: './redemption-records/redemption-records.module#RedemptionRecordsModule',
    resolve: {
      redemptions: RedemptionsResolver,
    },
    canActivate: [AuthGuard, AdminGuard]
  },
  { path: 'not-found', component: NotFoundComponent },
  { path: 'error', component: ErrorComponent },
  { path: '**', redirectTo: '/not-found' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes), CoreModule],
  exports: [RouterModule],
  providers: [UsersResolver],
})
export class AppRoutingModule { }
