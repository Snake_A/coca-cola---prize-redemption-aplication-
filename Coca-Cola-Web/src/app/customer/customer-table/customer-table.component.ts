import { Component, OnInit, ViewChildren, QueryList, Input, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { Observable } from 'rxjs';
import { Customer } from 'src/app/models/customer';
import { CustomerTableService } from './customer-table.service';
import { NgbdSortableHeader, SortEvent } from '../../directives/sortable.directive';
import { Router } from '@angular/router';

@Component({
  selector: 'app-customer-table',
  templateUrl: './customer-table.component.html',
  styleUrls: ['./customer-table.component.css']
})
export class CustomerTableComponent implements OnInit, OnChanges {
  @Input()
  customers: Customer[];
  customers$: Observable<Customer[]>;
  total$: Observable<number>;
  editButton = false;
  @Output()
  status = new EventEmitter();
  @Output()
  deleteEvent = new EventEmitter<string>();

  @ViewChildren(NgbdSortableHeader) headers: QueryList<NgbdSortableHeader>;

  constructor(
    public service: CustomerTableService,
    public router: Router,
  ) {
  }

  onSort({ column, direction }: SortEvent) {
    // resetting other headers
    this.headers.forEach(header => {
      if (header.sortable !== column) {
        header.direction = '';
      }
    });

    this.service.sortColumn = column;
    this.service.sortDirection = direction;
  }

  selectCustomer(customerId: string) {
    this.router.navigate(['customers', customerId, 'outlets']);
  }

  toggleButton(customerId: string) {
    this.editButton = !this.editButton;
    this.status.emit({
      status: this.editButton,
      id: customerId,
    });
  }

  deleteCustomer(customerId: string) {
    if (confirm(`Are you sure you want to delete
customer with ID ${customerId}`)) {
      this.deleteEvent.emit(customerId);
    }
  }

  ngOnInit() {
    this.service.customers = this.customers;
    this.customers$ = this.service.customers$;
    this.total$ = this.service.total$;
  }

  ngOnChanges(changes: SimpleChanges) {
    this.customers = changes.customers.currentValue;
    this.service.customers = this.customers;
    this.service.reset();
    this.customers$ = this.service.customers$;
    this.total$ = this.service.total$;
  }
}
