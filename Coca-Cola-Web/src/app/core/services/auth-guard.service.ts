import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { AuthService } from './auth.service';
import { ToastrService } from 'ngx-toastr';
@Injectable()
export class AuthGuardService implements CanActivate {

  constructor(
    private authService: AuthService,
    private router: Router,
    private toastr: ToastrService,
  ) {}

  canActivate(): boolean {
    if (!this.authService.isAuthenticated()) {
      this.toastr.error('You need to be logged in!');
      this.router.navigate([`/login`]);
      return false;
    }
    return true;
  }
}
