import { Injectable } from '@angular/core';
import { StorageService } from './storage.service';
import { Observable, BehaviorSubject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { JwtHelperService } from '@auth0/angular-jwt';
import { tap } from 'rxjs/operators';
import { JWTService } from './jwt.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private readonly userSubject$ = new BehaviorSubject<string | null>(this.username);

  constructor(
    private readonly storage: StorageService,
    private readonly jwtHelper: JwtHelperService,
    private readonly http: HttpClient,
    private readonly jwtService: JWTService,
  ) { }

  public isAuthenticated(): boolean {
    const token = this.storage.get('token');
    // Check whether the token is expired and return
    // true or false
    if (!!token) {
      if (!this.jwtHelper.isTokenExpired(token)) {
        this.userSubject$.next(this.username);

        return true;
      } else {
        this.storage.remove('token');
        this.storage.remove('username');
        this.storage.remove('role');
        this.userSubject$.next(this.username);

        return false;
      }
    }
    this.userSubject$.next(this.username);

    return false;
  }

  public get user$() {
    return this.userSubject$.asObservable();
  }

  private get username(): string | null {
    const token = this.storage.get('token');
    const username = this.storage.get('username') || '';
    if (token) {

      return username;
    }

    return null;
  }

  public login(username: string, password: string): Observable<any> {
    return this.http.post('http://localhost:3000/session', { username, password }).pipe(
      tap((res: any) => {
        this.storage.set('token', res.token);
        const user = this.decodeToken(res.token);
        this.storage.set('username', user.username);
        this.storage.set('role', user.roles[0].name);
        this.userSubject$.next(this.username);
      }
      ));
  }

  public logout(): Observable<any> {
    const token = this.storage.remove('token');
    const username = this.storage.remove('username');
    const role = this.storage.remove('role');
    this.userSubject$.next(this.username);
    return this.http.delete('http://localhost:3000/session', {});
  }

  private decodeToken(token: string): any {
    return this.jwtService.JWTDecoder(token);
  }
}
