import { Injectable } from '@angular/core';
import { User } from 'src/app/models/user';
import { Outlet } from '../models/outlet';

@Injectable({
    providedIn: 'root'
})
export class UserService {
    public users;
    private outletsResolve;


    public getData(users, route) {
        if (users.users) {
            this.users = users.users;
            this.outletsResolve = users.outletsResolve;
        }
        if (users.outlets) {
            const outlet = users.outlets.find((outletData: Outlet) => outletData.id === route.snapshot.params.id);
            this.users = outlet.users;
            this.users.forEach((user: User) => {
                user.outlet = outlet;
                user.customer = outlet.customer;
            });
            this.outletsResolve = users.outlets;
        }
        return {users: this.users, outletsResolve: this.outletsResolve};
    }
}
