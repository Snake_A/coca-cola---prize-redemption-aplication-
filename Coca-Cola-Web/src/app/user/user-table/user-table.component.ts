import { Component, OnInit, ViewChildren, QueryList, Input, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from 'src/app/models/user';
import { UserTableService } from './user-table.service';
import { NgbdSortableHeader, SortEvent } from '../../directives/sortable.directive';
import { Outlet } from '../../models/outlet';
import { UserRegister } from '../../models/user-register';
import { UserEdit } from '../../models/user-edit';

@Component({
  selector: 'app-user-table',
  templateUrl: './user-table.component.html',
  styleUrls: ['./user-table.component.css']
})
export class UserTableComponent implements OnInit, OnChanges {
  @Input()
  users: User[];
  @Input()
  outletsResolve: Outlet[];
  @Output()
  deleteEvent = new EventEmitter<string>();
  @Output()
  userCreatedEvent = new EventEmitter<UserRegister>();
  @Output()
  userEditedEvent = new EventEmitter<any>();
  users$: Observable<User[]>;
  total$: Observable<number>;
  create = false;
  userToEdit: User;
  edit = false;

  @ViewChildren(NgbdSortableHeader) headers: QueryList<NgbdSortableHeader>;

  constructor(public service: UserTableService) {
  }

  onSort({ column, direction, sub }: SortEvent) {
    // resetting other headers
    this.headers.forEach(header => {
      if (header.sortable !== column) {
        header.direction = '';
      }
    });

    this.service.sortColumn = column;
    this.service.sortDirection = direction;
    this.service.sub = sub;
  }

  deleteUser(userId: string) {
    if (confirm(`Are you sure you want to delete
user with ID ${userId}`)) {
      this.deleteEvent.emit(userId);
    }
  }

  ngOnInit() {
    this.service.users = this.users;
    this.users$ = this.service.users$;
    this.total$ = this.service.total$;
  }

  ngOnChanges(changes: SimpleChanges) {
    this.users = changes.users.currentValue;
    this.service.users = this.users;
    this.service.reset();
    this.users$ = this.service.users$;
    this.total$ = this.service.total$;
  }

  userCreated(user: UserRegister) {
    this.userCreatedEvent.emit(user);
  }

  toggleCreate() {
    this.create = !this.create;
  }

  toggleEdit(user: User) {
    this.userToEdit = user;
    this.edit = !this.edit;
  }

  cancelEdit() {
    this.edit = !this.edit;
  }

  editUser(data: {user: UserEdit, id: string}) {
    this.userEditedEvent.emit(data);
  }
}
