import { Component, OnInit, ViewChildren, QueryList, Input, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { Observable } from 'rxjs';
import { Outlet } from 'src/app/models/outlet';
import { OutletTableService } from './outlet-table.service';
import { NgbdSortableHeader, SortEvent } from '../../directives/sortable.directive';
import { Router } from '@angular/router';
import { OutletCreate } from 'src/app/models/outlet-create';
import { OutletEdit } from 'src/app/models/outlet-edit';
import { Customer } from 'src/app/models/customer';

@Component({
  selector: 'app-outlet-table',
  templateUrl: './outlet-table.component.html',
  styleUrls: ['./outlet-table.component.css']
})
export class OutletTableComponent implements OnInit, OnChanges {
  @Input()
  outlets: Outlet[];
  @Input()
  customers: any;
  outlets$: Observable<Outlet[]>;
  total$: Observable<number>;
  @Output()
  deleteEvent = new EventEmitter<string>();
  @Output()
  outletCreatedEvent = new EventEmitter<any>();
  @Output()
  outletEditedEvent = new EventEmitter<any>();
  create = false;
  outletToEdit: Outlet;
  edit = false;

  @ViewChildren(NgbdSortableHeader) headers: QueryList<NgbdSortableHeader>;

  constructor(
    public service: OutletTableService,
    public router: Router,
  ) {
  }

  onSort({ column, direction, sub }: SortEvent) {
    // resetting other headers
    this.headers.forEach(header => {
      if (header.sortable !== column) {
        header.direction = '';
      }
    });

    this.service.sortColumn = column;
    this.service.sortDirection = direction;
    this.service.sub = sub;
  }

  selectOutlet(outletId: string) {
    this.router.navigate(['outlets', outletId, 'users']);
  }

  deleteOutlet(outletId: string) {
    if (confirm(`Are you sure you want to delete
outlet with ID ${outletId}`)) {
      this.deleteEvent.emit(outletId);
    }
  }

  ngOnInit() {
    this.service.outlets = this.outlets;
    this.outlets$ = this.service.outlets$;
    this.total$ = this.service.total$;
  }

  ngOnChanges(changes: SimpleChanges) {
    this.outlets = changes.outlets.currentValue;
    this.service.outlets = this.outlets;
    this.service.reset();
    this.outlets$ = this.service.outlets$;
    this.total$ = this.service.total$;
  }

  outletCreated(data: any) {
    this.outletCreatedEvent.emit(data);
  }

  toggleCreate() {
    this.create = !this.create;
  }

  toggleEdit(outlet: Outlet) {
    this.outletToEdit = outlet;
    this.edit = !this.edit;
  }

  cancelEdit() {
    this.edit = !this.edit;
  }

  editOutlet(data: { outlet: OutletEdit, id: string }) {
    this.outletEditedEvent.emit(data);
  }
}
