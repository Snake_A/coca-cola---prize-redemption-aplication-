import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { MustMatch } from '../../validators/must-match.validator';
import { CustomValidators } from '../../validators/register-validator';
import { OutletEdit } from '../../models/outlet-edit';
import { Outlet } from 'src/app/models/outlet';

@Component({
  selector: 'app-outlet-edit',
  templateUrl: 'outlet-edit.component.html',
  styleUrls: ['outlet-edit.component.css']
})
export class OutletEditComponent implements OnInit {
  @Input()
  outletData: Outlet;
  @Output()
  outletEditedEvent = new EventEmitter<any>();
  @Output()
  cancelEdit = new EventEmitter();
  registerForm: FormGroup;
  submitted = false;
  id: string;

  constructor(private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.id = this.outletData.id;
    this.registerForm = this.formBuilder.group({
      name: [this.outletData.name, [Validators.required, Validators.minLength(2)]],
      address: [this.outletData.address, [Validators.required, Validators.minLength(2)]]
    });
  }

  get f() { return this.registerForm.controls; }

  onSubmit() {
    this.submitted = true;

    if (this.registerForm.invalid) {
      return;
    }

    const outlet: OutletEdit = {
      name: this.registerForm.value.name,
      address: this.registerForm.value.address,
    };

    this.outletEditedEvent.emit({outlet, id: this.outletData.id});
  }

  cancelEditing() {
    this.cancelEdit.emit();
  }

}
