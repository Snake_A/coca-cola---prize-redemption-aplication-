import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OutletTableComponent } from './outlet-table/outlet-table.component';
import { SharedModule } from '../shared/shared.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { OutletComponent } from './outlet.component';
import { OutletRoutingModule } from './outlet-routing.module';
import { OutletCreateComponent } from './outlet-create/outlet-create.component';
import { SelectDropDownModule } from 'ngx-select-dropdown';
import { OutletEditComponent } from './outlet-edit/outlet-edit.component';

@NgModule({
  declarations: [OutletTableComponent, OutletComponent, OutletCreateComponent, OutletEditComponent],
  imports: [
    OutletRoutingModule,
    CommonModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    SelectDropDownModule,
  ]
})
export class OutletModule { }
