import { Component, OnInit, OnDestroy } from '@angular/core';
import { Redemption } from '../models/redemption';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-redemption-records',
  templateUrl: './redemption-records.component.html',
  styleUrls: ['./redemption-records.component.css']
})
export class RedemptionRecordsComponent implements OnInit, OnDestroy {
  redemptions: Redemption[] = [];

  private redemptionsSubscription: Subscription;

  constructor(
    private route: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.redemptionsSubscription = this.route.data.subscribe(
      (redemptions: { redemptions: Redemption[] }) => {
        this.redemptions = redemptions.redemptions;
      });
  }

  ngOnDestroy() {
    if (this.redemptionsSubscription) {
      this.redemptionsSubscription.unsubscribe();
    }
  }
}
