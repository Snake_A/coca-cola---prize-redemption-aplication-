import { Outlet } from './outlet';
import { Customer } from './customer';

export interface User {
    id: string;
    username: string;
    firstName: string;
    lastName: string;
    outlet: Outlet;
    customer: Customer;
    dateCreated: string;
}
