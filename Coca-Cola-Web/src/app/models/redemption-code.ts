import { RedemptionCodeStatus } from './enums/redemption-code-status-type';
import { Outlet } from './outlet';
import { Customer } from './customer';
import { User } from './user';

export interface RedemptionCode {
    code: string;
    status: RedemptionCodeStatus;
    redeemedAt: Date;
    redeemedAtOutlet: Outlet;
    redeemedFrom: User;
    redeemedFromCustomer: Customer;
    dateCreated: Date;
    reported: boolean;
}
